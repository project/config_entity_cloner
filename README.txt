CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * How it works
 * Maintainers


INTRODUCTION
------------
The Config Entity Cloner module allows site builder to clone entity
any configuration entity.

This modules adds the 'clone' button in every configuration entity
operation list. Clicking on this button allows you to access to the
clone form. Once click, each  declared configuration is duplicated.

You can create your on duplication process with a specific plugin.
You can create new configuration process with the command :
`drush generate config-entity-cloner-process`
Then you have to complete the process `cloneProcess` method.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Env Libraries module as you would normally install
a contributed Drupal module. Visit https://www.drupal.org/node/1897420
for further
information.


HOW IT WORKS
------------

The clone process is based on a succession of process. Foreach entity
the Config Entity Cloner will execute each process. By defautl the list of
process is :
- fields configuration
- form display configuration
- view display configuration
- translation configuration
- simple sitemap configuration.

As you can see, each process depends on a configuration type (fields,
form display etc...). Some of configuration may not be able for some
entity types. It is not a problem as we duplicate existing configuration,
the module does not create unexisting configuration. In this context,
the module does not process unexisting duplication.


GENERATE PROCESS PLUGIN
------------

A Config Entity Cloner Process is plugin. So you can create your own
process according to your own configuration. This process should duplicate
an existing configuration using the `cloneProcess` method.

In order to create your process you can use the drush generate command :
`drush generate config-entity-cloner-process`


GENERATE PROCESSOR PLUGIN
------------

By default, Config Entity Cloner will execute the whole list of process.
If the entity type you are about to clone does not need to execute the
list of all the process, you can restrict the list of process to execute
using a specific Config Entity Cloner Processor. A processor is a plugin
which id matches a specific entity type id. This way, you can create your
own processor that will define a specific list of process with the method
`getProcessList` that should return a list of process.

In order to create your processor you can use the drush generate command :
`drush generate config-entity-cloner-processor`


MAINTAINERS
-----------

 * Thomas Sécher - https://www.drupal.org/u/tsecher

Supporting organization:

 * Lycanthrop - https://www.drupal.org/lycanthrop
