<?php

namespace Drupal\config_entity_cloner\Plugin\config_entity_cloner\ConfigEntityClonerProcess;

use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface;
use Drupal\config_entity_cloner\Service\ConfigEntityCloner;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @ConfigEntityClonerProcessAnnotation(
 *   id = "simple_site_map",
 *   label = "Simple SiteMap",
 *   weight = 30
 * )
 */
class SimpleSitemapConfigEntityClonerProcess implements ConfigEntityClonerProcessInterface {

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * SimpleSiteMapCloneProcess constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   THe config factory.
   */
  public function __construct(LoggerInterface $logger, ConfigFactoryInterface $configFactory) {
    $this->logger = $logger;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigEntityClonerProcessInterface {
    return new static(
      $container->get('logger.factory')->get(ConfigEntityCloner::SERVICE_NAME),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function cloneProcess(EntityInterface $newEntity, EntityInterface $originalEntity): void {
    /** @var \Drupal\simple_sitemap\Manager\Generator $simpleSiteMapGenerator */
    $simpleSiteMapGenerator = \Drupal::service('simple_sitemap.generator');
    if ($simpleSiteMapGenerator) {
      $retro = method_exists($simpleSiteMapGenerator, 'getBundleSettings');
      $bundle = $originalEntity->getEntityType()->getBundleOf();
      $bundleManager = $retro ? $simpleSiteMapGenerator : $simpleSiteMapGenerator->entityManager();
      if ($data = $bundleManager->getBundleSettings($bundle, $originalEntity->id())) {
        $bundle = isset($data[$bundle]) ? $bundle : "default";
        if ($retro) {
          $bundleManager->setBundleSettings($bundle, $newEntity->id(), $data);
        }
        else {
          $bundleManager
            ->enableEntityType($newEntity->getEntityType()->getBundleOf())
            // All following operations will concern these variants.
            ->setVariants(array_keys($data))
            ->setBundleSettings($newEntity->getEntityType()->getBundleOf(), $newEntity->id(), reset($data));
        }
      }
    }
  }

}
