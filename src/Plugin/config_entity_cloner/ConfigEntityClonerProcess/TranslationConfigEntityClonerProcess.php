<?php

namespace Drupal\config_entity_cloner\Plugin\config_entity_cloner\ConfigEntityClonerProcess;

use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface;
use Drupal\config_entity_cloner\Service\ConfigEntityCloner;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @ConfigEntityClonerProcessAnnotation(
 *   id = "translation_clone_process",
 *   label = "Clone translation",
 *   weight = 20
 * )
 */
class TranslationConfigEntityClonerProcess implements ConfigEntityClonerProcessInterface {

  /**
   * Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * CloneFieldsProcess constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerInterface $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigEntityClonerProcessInterface {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get(ConfigEntityCloner::SERVICE_NAME)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function cloneProcess(EntityInterface $newEntity, EntityInterface $originalEntity): void {

    try {
      $entityTypeId = $originalEntity->getEntityType()->getBundleOf();
      $languageContentSettings = $this->entityTypeManager->getStorage('language_content_settings');
      if ($initialLanguageConf = $languageContentSettings->load($entityTypeId . '.' . $originalEntity->id())) {
        $data = $initialLanguageConf->toArray();
        $data['uuid'] = \Drupal::service('uuid')->generate();
        $data['id'] = $entityTypeId . '.' . $newEntity->id();
        $data['target_bundle'] = $newEntity->id();

        $languageContentSettings
          ->create($data)
          ->save();
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

}
