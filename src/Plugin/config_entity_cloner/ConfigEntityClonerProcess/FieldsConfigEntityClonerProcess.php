<?php

namespace Drupal\config_entity_cloner\Plugin\config_entity_cloner\ConfigEntityClonerProcess;

use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface;
use Drupal\config_entity_cloner\Service\ConfigEntityCloner;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @ConfigEntityClonerProcessAnnotation(
 *   id = "fields_clone_process",
 *   label = "Clone fields",
 *   weight = 0
 * )
 */
class FieldsConfigEntityClonerProcess implements ConfigEntityClonerProcessInterface {

  /**
   * Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * CloneFieldsProcess constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field maanger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager, ConfigFactoryInterface $configFactory, LoggerInterface $logger) {
    $this->entityFieldManager = $entityFieldManager;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigEntityClonerProcessInterface {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('logger.factory')->get(ConfigEntityCloner::SERVICE_NAME)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function cloneProcess(EntityInterface $newEntity, EntityInterface $originalEntity): void {
    $entityTypeId = $originalEntity->getEntityType()->getBundleOf();
    $originalFields = $this->entityFieldManager->getFieldDefinitions(
      $entityTypeId,
      $originalEntity->id());

    foreach ($originalFields as $fieldName => $originalField) {
      if (FieldStorageConfig::loadByName($entityTypeId, $fieldName)) {
        $this->duplicateField($newEntity, $originalField);
      }
      elseif ($originalField instanceof BaseFieldOverride) {
        $this->duplicateProperty($newEntity, $originalEntity, $originalField, $entityTypeId);
      }
    }
  }

  /**
   * Duplicate entity field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $newEntity
   *   The new entity.
   * @param \Drupal\field\FieldConfigInterface $originalField
   *   THe original entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function duplicateField(EntityInterface $newEntity, FieldConfigInterface $originalField) {
    $fieldName = $originalField->getName();
    $field_storage = FieldStorageConfig::loadByName($newEntity->getEntityTypeId(), $fieldName);
    $field = FieldConfig::loadByName($newEntity->getEntityTypeId(), $newEntity->id(), $fieldName);

    // Init data.
    $data = $originalField->toArray();
    unset($data['id']);
    $data['uuid'] = \Drupal::service('uuid')->generate();
    $data['field_storage'] = $field_storage;
    $data['bundle'] = $newEntity->id();
    $data['label'] = $originalField->label();

    if (empty($field)) {
      try {
        $fieldConfig = FieldConfig::create($data);
        $fieldConfig->save();
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }
  }

  /**
   * Duplicate property.
   *
   * @param \Drupal\Core\Entity\EntityInterface $newEntity
   *   The new entity.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   THe original entity.
   * @param \Drupal\Core\Field\Entity\BaseFieldOverride $field
   *   THe field.
   * @param string $entityTypeId
   *   The entity type id.
   */
  protected function duplicateProperty(EntityInterface $newEntity, EntityInterface $originalEntity, BaseFieldOverride $field, $entityTypeId) {
    $initialConfName = 'core.base_field_override.' . $entityTypeId . '.' . $originalEntity->id() . '.' . $field->getName();
    $newConfName = 'core.base_field_override.' . $entityTypeId . '.' . $newEntity->id() . '.' . $field->getName();

    $initialConfig = \Drupal::config($initialConfName)->getRawData();

    if (!empty($initialConfig)) {
      $data = $initialConfig;
      $data['uuid'] = \Drupal::service('uuid')->generate();
      $data['id'] = $entityTypeId . '.' . $newEntity->id() . '.' . $field->getName();
      $data['bundle'] = $newEntity->id();

      try {
        $this->configFactory
          ->getEditable($newConfName)
          ->setData($data)
          ->save();
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }
  }

}
