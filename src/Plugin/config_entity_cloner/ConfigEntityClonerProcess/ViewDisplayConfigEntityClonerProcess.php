<?php

namespace Drupal\config_entity_cloner\Plugin\config_entity_cloner\ConfigEntityClonerProcess;

use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface;
use Drupal\config_entity_cloner\Service\ConfigEntityCloner;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @ConfigEntityClonerProcessAnnotation(
 *   id = "view_display",
 *   label = "View Display",
 *   weight = 10
 * )
 */
class ViewDisplayConfigEntityClonerProcess implements ConfigEntityClonerProcessInterface {

  /**
   * EntityType Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * FormDisplayCloneProcess constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entityTypeManager) {
    $this->logger = $logger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigEntityClonerProcessInterface {
    return new static(
      $container->get('logger.factory')->get(ConfigEntityCloner::SERVICE_NAME),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function cloneProcess(EntityInterface $newEntity, EntityInterface $originalEntity): void {
    if ($viewDisplayStorage = $this->entityTypeManager->getStorage('entity_view_display')) {
      $viewDisplays = $viewDisplayStorage->loadByProperties(
        [
          'targetEntityType' => $originalEntity->getEntityType()->getBundleOf(),
          'bundle'           => $originalEntity->id(),
        ]
      );
      foreach ($viewDisplays as $viewDisplay) {
        /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $newViewDisplay */
        $newViewDisplay = $viewDisplay->createDuplicate();
        $newViewDisplay->set('bundle', $newEntity->id());
        try {
          $newViewDisplay->save();
        }
        catch (\Exception $e) {
          $this->logger->error($e->getMessage());
        }
      }
    }
  }

}
