<?php

namespace Drupal\config_entity_cloner\Plugin\config_entity_cloner\ConfigEntityClonerProcessor;

use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessPluginManager;
use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation For the type processor.
 *
 * @ConfigEntityClonerProcessorAnnotation(
 *   id = "default",
 *   label = "Default",
 *   weight = 0
 * )
 */
class DefaultConfigEntityClonerProcessor implements ConfigEntityClonerProcessorInterface {

  /**
   * The cloner process plugin manager.
   *
   * @var \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessPluginManager
   */
  protected ConfigEntityClonerProcessPluginManager $clonerProcessPluginManager;

  /**
   * Default constructor.
   */
  public function __construct(ConfigEntityClonerProcessPluginManager $cloner_process_plugin_manager) {
    $this->clonerProcessPluginManager = $cloner_process_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigEntityClonerProcessorInterface {
    return new static(
      $container->get(ConfigEntityClonerProcessPluginManager::SERVICE_NAME)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessList(): array {
    return $this->clonerProcessPluginManager->getAllPlugins();
  }

}
