<?php

namespace Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor;

/**
 * Plugin wrapper for config entity cloner processors.
 *
 * @package Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor
 */
class ConfigEntityClonerProcessorPluginWrapper {

  /**
   * Plugin.
   *
   * @var \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorInterface
   */
  protected $plugin;

  /**
   * Label.
   *
   * @var string
   */
  protected $label;

  /**
   * ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Weight..
   *
   * @var int
   */
  protected $weight;

  /**
   * ConfigEntityClonerProcessorPluginWrapper constructor.
   *
   * @param \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorInterface $plugin
   *   The plugin.
   * @param string $label
   *   The plugin label.
   * @param string $id
   *   The plugin id.
   * @param int $weight
   *   The plugin weight.
   */
  public function __construct(ConfigEntityClonerProcessorInterface $plugin, string $label, string $id, int $weight) {
    $this->plugin = $plugin;
    $this->label = $label;
    $this->id = $id;
    $this->weight = $weight;
  }

  /**
   * The plugin.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorInterface
   *   The plugin.
   */
  public function getPlugin(): ConfigEntityClonerProcessorInterface {
    return $this->plugin;
  }

  /**
   * The plugin name.
   *
   * @return string
   *   THe plugin name.
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * The plugin id.
   *
   * @return string
   *   THe plugin id.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * The weight.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int {
    return $this->weight;
  }

}
