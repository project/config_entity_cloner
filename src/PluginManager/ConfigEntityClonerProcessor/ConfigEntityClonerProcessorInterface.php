<?php

namespace Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface for config entity cloner processors.
 *
 * @package Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor
 */
interface ConfigEntityClonerProcessorInterface {

  /**
   * Create the ConfigEntityClonerProcessor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorInterface
   *   The ConfigEntityClonerProcessor.
   */
  public static function create(ContainerInterface $container): ConfigEntityClonerProcessorInterface;

  /**
   * Return the list of process to execute.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface[]
   *   The list of process.
   */
  public function getProcessList(): array;

}
