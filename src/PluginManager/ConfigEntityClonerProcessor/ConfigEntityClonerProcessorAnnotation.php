<?php

namespace Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor;

use Drupal\Component\Annotation\Plugin;

/**
 * Define a ConfigEntityClonerProcessor annotation object.
 *
 * @Annotation
 *
 * @ingroup config_entity_cloner_processor
 */
class ConfigEntityClonerProcessorAnnotation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The process label.
   *
   * @var string
   */
  public $label;

  /**
   * The process weight.
   *
   * @var int
   */
  public $weight;

}
