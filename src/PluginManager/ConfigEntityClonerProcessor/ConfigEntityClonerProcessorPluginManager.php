<?php

namespace Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for config entity cloner processors.
 *
 * @package Drupal\config_entity_cloner\PluginManager
 */
class ConfigEntityClonerProcessorPluginManager extends DefaultPluginManager {

  /**
   * Package Name.
   *
   * @const string
   */
  const PACKAGE_NAME = 'config_entity_cloner/ConfigEntityClonerProcessor';

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'config_entity_cloner.config_entity_cloner_processor_plugin_manager';

  /**
   * List of available plugin.
   *
   * @var \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorPluginWrapper[]
   */
  protected $plugins;

  /**
   * Singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/' . self::PACKAGE_NAME,
      $namespaces,
      $module_handler,
      ConfigEntityClonerProcessorInterface::class,
      ConfigEntityClonerProcessorAnnotation::class);

    $hookName = 'config_entity_cloner_processor_service_info';
    $this->alterInfo($hookName);
    $this->setCacheBackend($cache_backend, $hookName);

    $this->cacheTags[] = self::PACKAGE_NAME;
  }

  /**
   * Return all available plugins.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorPluginWrapper[]
   *   The list of plugins.
   */
  public function getAllPlugins(): array {
    if (is_null($this->plugins)) {
      foreach ($this->getDefinitions() as $definition) {
        $this->plugins[$definition['id']] = new ConfigEntityClonerProcessorPluginWrapper(
          static::createPluginInstance($definition['id'], $definition),
          $definition['label'],
          $definition['id'],
          $definition['weight']
        );
      }

      // Sort by weight.
      uasort($this->plugins, function (ConfigEntityClonerProcessorPluginWrapper $a, ConfigEntityClonerProcessorPluginWrapper $b) {
        $a = $a->getWeight();
        $b = $b->getWeight();
        if ($a > $b) {
          return 1;
        }
        elseif ($a < $b) {
          return -1;
        }

        return 0;
      });
    }

    return $this->plugins;
  }

  /**
   * {@inheritdoc}
   */
  protected static function createPluginInstance($plugin_id, array $configuration = []) {
    return call_user_func($configuration['class'] . '::create', \Drupal::getContainer());
  }

  /**
   * Return the plugin.
   *
   * @param string $id
   *   The plugin id.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorPluginWrapper|null
   *   The plugin wrapper.
   */
  public function getPluginWrapperById($id) {
    if (is_null($this->plugins)) {
      $this->getAllPlugins();
    }

    return $this->plugins[$id] ?? NULL;
  }

  /**
   * Return the Processor according to entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorInterface
   *   The processor.
   */
  public function getProcessorByEntity(EntityInterface $entity): ConfigEntityClonerProcessorInterface {
    $allPlugins = $this->getAllPlugins();
    $entityTypeId = $entity->getEntityTypeId();
    $wrapper = $allPlugins[$entityTypeId] ?? $allPlugins['default'];

    return $wrapper->getPlugin();
  }

}
