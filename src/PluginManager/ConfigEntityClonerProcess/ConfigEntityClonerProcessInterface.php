<?php

namespace Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface for config entity clonser process.
 *
 * @package Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess
 */
interface ConfigEntityClonerProcessInterface {

  /**
   * Create the ConfigEntityClonerProcess.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface
   *   The ConfigEntityClonerProcess.
   */
  public static function create(ContainerInterface $container): ConfigEntityClonerProcessInterface;

  /**
   * Clone process.
   *
   * @param \Drupal\Core\Entity\EntityInterface $newEntity
   *   The new entity.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original Entity.
   */
  public function cloneProcess(EntityInterface $newEntity, EntityInterface $originalEntity): void;

}
