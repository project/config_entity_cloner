<?php

namespace Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess;

/**
 * Plugin wrapper for config entity cloner process.
 *
 * @package Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess
 */
class ConfigEntityClonerProcessPluginWrapper {

  /**
   * Plugin.
   *
   * @var \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface
   */
  protected $plugin;

  /**
   * Label.
   *
   * @var string
   */
  protected $label;

  /**
   * ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Weight..
   *
   * @var int
   */
  protected $weight;

  /**
   * ConfigEntityClonerProcessPluginWrapper constructor.
   *
   * @param \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface $plugin
   *   The plugin.
   * @param string $label
   *   The plugin label.
   * @param string $id
   *   The plugin id.
   * @param int $weight
   *   The plugin weight.
   */
  public function __construct(ConfigEntityClonerProcessInterface $plugin, string $label, string $id, int $weight) {
    $this->plugin = $plugin;
    $this->label = $label;
    $this->id = $id;
    $this->weight = $weight;
  }

  /**
   * The plugin.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessInterface
   *   The plugin.
   */
  public function getPlugin(): ConfigEntityClonerProcessInterface {
    return $this->plugin;
  }

  /**
   * The plugin name.
   *
   * @return string
   *   THe plugin name.
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * The plugin id.
   *
   * @return string
   *   THe plugin id.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * The weight.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int {
    return $this->weight;
  }

}
