<?php

namespace Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for config entity cloner process.
 *
 * @package Drupal\config_entity_cloner\PluginManager
 */
class ConfigEntityClonerProcessPluginManager extends DefaultPluginManager {

  /**
   * Package Name.
   *
   * @const string
   */
  const PACKAGE_NAME = 'config_entity_cloner/ConfigEntityClonerProcess';

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'config_entity_cloner.config_entity_cloner_process_plugin_manager';

  /**
   * List of available plugin.
   *
   * @var \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessPluginWrapper[]
   */
  protected $plugins;

  /**
   * Singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/' . self::PACKAGE_NAME,
      $namespaces,
      $module_handler,
      ConfigEntityClonerProcessInterface::class,
      ConfigEntityClonerProcessAnnotation::class);

    $hookName = 'config_entity_cloner_process_service_info';
    $this->alterInfo($hookName);
    $this->setCacheBackend($cache_backend, $hookName);

    $this->cacheTags[] = self::PACKAGE_NAME;
  }

  /**
   * Return all available plugins.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessPluginWrapper[]
   *   The list of plugins.
   */
  public function getAllPlugins(): array {
    if (is_null($this->plugins)) {
      $this->plugins = [];
      foreach ($this->getDefinitions() as $definition) {
        $this->plugins[$definition['id']] = new ConfigEntityClonerProcessPluginWrapper(
          static::createPluginInstance($definition['id'], $definition),
          $definition['label'],
          $definition['id'],
          $definition['weight']
        );
      }

      // Sort by weight.
      uasort($this->plugins, function (ConfigEntityClonerProcessPluginWrapper $a, ConfigEntityClonerProcessPluginWrapper $b) {
        $a = $a->getWeight();
        $b = $b->getWeight();
        if ($a > $b) {
          return 1;
        }
        elseif ($a < $b) {
          return -1;
        }

        return 0;
      });
    }

    return $this->plugins;
  }

  /**
   * {@inheritdoc}
   */
  protected static function createPluginInstance($plugin_id, array $configuration = []) {
    return call_user_func($configuration['class'] . '::create', \Drupal::getContainer());
  }

  /**
   * Return the plugin.
   *
   * @param string $id
   *   The plugin id.
   *
   * @return \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessPluginWrapper|null
   *   The plugin wrapper.
   */
  public function getProcessById($id) {
    if (is_null($this->plugins)) {
      $this->getAllPlugins();
    }

    return $this->plugins[$id] ?? NULL;
  }

}
