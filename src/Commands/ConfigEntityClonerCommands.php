<?php

namespace Drupal\config_entity_cloner\Commands;

use Drupal\config_entity_cloner\Service\ConfigEntityCloner;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class ConfigEntityClonerCommands extends DrushCommands {

  /**
   * EntityConfig cloner.
   *
   * @var \Drupal\config_entity_cloner\Service\ConfigEntityCloner
   */
  protected $cloner;

  /**
   * ConfigEntityClonerCommands constructor.
   *
   * @param \Drupal\config_entity_cloner\Service\ConfigEntityCloner $cloner
   *   The cloner.
   */
  public function __construct(ConfigEntityCloner $cloner) {
    $this->cloner = $cloner;
  }

  /**
   * Duplicate entity.
   *
   * @param string $originalEntityTypeId
   *   The entity type id.
   * @param string $originalEntityId
   *   THe entity id.
   * @param array $options
   *   The options.
   *
   * @option new-entity-id
   *   The new entity id.
   * @option new-entity-label
   *   The new entity label.
   * @usage config_entity_cloner-commandName foo
   *   Usage description.
   *
   * @command config_entity_cloner:cloneConfigEntity
   * @aliases cec:cce
   */
  public function cloneConfigEntity(string $originalEntityTypeId, string $originalEntityId, array $options = [
    'new-entity-id'    => NULL,
    'new-entity-label' => NULL,
  ]) {
    $originalEntity = $this->cloner->getConfigEntityByTypeAndId($originalEntityTypeId, $originalEntityId);

    if (is_null($originalEntity)) {
      throw new \Exception('No original entity found. Cannot be cloned.');
    }

    $newId = $options['new-entity-id'] ?? $this->cloner->getDefaultCloneEntityId($originalEntity);
    $newLabel = $options['new-entity-id'] ?? $this->cloner->getDefaultCloneEntityLabel($originalEntity);

    $this->cloner->cloneConfiguration($newLabel, $newId, $originalEntity);
  }

}
