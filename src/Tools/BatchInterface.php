<?php

namespace Drupal\config_entity_cloner\Tools;

/**
 * Interface for batch tools.
 *
 * @package Drupal\config_entity_cloner\Tools
 */
interface BatchInterface {

  /**
   * Initialise the batch.
   */
  public function init();

  /**
   * Return the title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string;

  /**
   * Set the title.
   *
   * @param string $title
   *   The title.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function setTitle(string $title): BatchInterface;

  /**
   * Return the init message.
   *
   * @return string
   *   The initMessage.
   */
  public function getInitMessage(): string;

  /**
   * Set the init message.
   *
   * @param string $initMessage
   *   THe init message.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function setInitMessage(string $initMessage): BatchInterface;

  /**
   * Return The progress message.
   *
   * @return string
   *   The progress message.
   */
  public function getProgressMessage(): string;

  /**
   * Set the progress message.
   *
   * @param string $progressMessage
   *   The progress message.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function setProgressMessage(string $progressMessage): BatchInterface;

  /**
   * Get the error message.
   *
   * @return string
   *   The error message.
   */
  public function getErrorMessage(): string;

  /**
   * Set the error message.
   *
   * @param string $errorMessage
   *   The error message.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function setErrorMessage(string $errorMessage): BatchInterface;

  /**
   * Get the common data.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   The common data.
   */
  public function getCommonBatchData(string $key = NULL);

  /**
   * Set the common batch data.
   *
   * @param string $key
   *   The common Batch data key.
   * @param array $commonBatchData
   *   The common Batch data.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function addCommonBatchData(string $key, array $commonBatchData): BatchInterface;

  /**
   * Add an operation.
   *
   * @param array|string $callback
   *   The callback.
   * @param array $params
   *   The params.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function addOperation($callback, array $params = []): BatchInterface;

  /**
   * Add a finish callback.
   *
   * @param array|string $callback
   *   THe callback.
   * @param array $params
   *   THe params.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function addFinishCallback($callback, array $params = []): BatchInterface;

  /**
   * Set the redirection callback.
   *
   * @param array|string $callback
   *   THe callback.
   * @param array $params
   *   THe params.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   Return the batch.
   */
  public function setRedirectionCallback($callback, array $params = []): BatchInterface;

}
