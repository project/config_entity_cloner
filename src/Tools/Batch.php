<?php

namespace Drupal\config_entity_cloner\Tools;

/**
 * Provides tools for batch.
 *
 * @package Drupal\config_entity_cloner\Tools
 */
class Batch implements BatchInterface {

  /**
   * Title.
   *
   * @var string
   */
  protected $title;

  /**
   * Init message.
   *
   * @var string
   */
  protected $initMessage = 'Start';

  /**
   * Progress message.
   *
   * @var string
   */
  protected $progressMessage = 'Processing.';

  /**
   * Error Message.
   *
   * @var string
   */
  protected $errorMessage = 'An error occurred during processing';

  /**
   * Finish Callbacks.
   *
   * @var array
   */
  protected $finishCallbacks = [];

  /**
   * Redirection Callback.
   *
   * @var array
   */
  protected $redirectionCallback = [];

  /**
   * OPerations list.
   *
   * @var array
   */
  protected $operations;

  /**
   * Mutability state.
   *
   * @var bool
   */
  protected $isMutable = TRUE;

  /**
   * Common batch data.
   *
   * @var array
   */
  protected $commonBatchData = [];

  /**
   * Batch constructor.
   *
   * @param string $title
   *   The title.
   * @param string $initMessage
   *   The init message.
   * @param string $progressMessage
   *   The progress message.
   * @param string $errorMessage
   *   The error message.
   */
  public function __construct(string $title, string $initMessage = NULL, string $progressMessage = NULL, string $errorMessage = NULL) {
    $this->title = $title;
    $this->initMessage = $initMessage ?: $this->initMessage;
    $this->progressMessage = $progressMessage ?: $this->progressMessage;
    $this->errorMessage = $errorMessage ?: $this->errorMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function init() {
    if (!empty($this->operations)) {
      $batch = [
        'title'            => $this->getTitle(),
        'init_message'     => $this->getInitMessage(),
        'progress_message' => $this->getProgressMessage(),
        'error_message'    => $this->getErrorMessage(),
        'finished'         => [$this, 'onBatchFinished'],
        'operations'       => $this->getOperationsList(),
      ];

      $this->isMutable = FALSE;

      batch_set($batch);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): BatchInterface {
    $this->checkMutability();
    $this->title = $title;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getInitMessage(): string {
    return $this->initMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function setInitMessage(string $initMessage): BatchInterface {
    $this->checkMutability();
    $this->initMessage = $initMessage;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProgressMessage(): string {
    return $this->progressMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function setProgressMessage(string $progressMessage): BatchInterface {
    $this->checkMutability();
    $this->progressMessage = $progressMessage;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorMessage(): string {
    return $this->errorMessage;
  }

  /**
   * {@inheritdoc}
   */
  public function setErrorMessage(string $errorMessage): BatchInterface {
    $this->checkMutability();
    $this->errorMessage = $errorMessage;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addOperation($callback, array $params = []): BatchInterface {
    $this->checkMutability();
    $this->operations[] = [
      'callback' => $callback,
      'params'   => $params,
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addFinishCallback($callback, array $params = []): BatchInterface {
    $this->checkMutability();
    $this->finishCallbacks[] = [
      'callback' => $callback,
      'params'   => $params,
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setRedirectionCallback($callback, array $params = []): BatchInterface {
    $this->checkMutability();
    $this->redirectionCallback = [
      'callback' => $callback,
      'params'   => $params,
    ];

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommonBatchData(string $key = NULL) {
    if (is_null($key)) {
      return $this->commonBatchData;
    }
    elseif (isset($this->commonBatchData[$key])) {
      return $this->commonBatchData[$key];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function addCommonBatchData(string $key, array $commonBatchData): BatchInterface {
    $this->commonBatchData[$key] = $commonBatchData;

    return $this;
  }

  /**
   * Check the batch mutability.
   *
   * @throws \Exception
   */
  protected function checkMutability() {
    if (!$this->isMutable) {
      throw new \Exception('The batch has been initialized. Updated data after initialisation will not affect the batch process.');
    }
  }

  /**
   * ON bactch finished.
   *
   * @param bool $success
   *   The succes status.
   * @param array $results
   *   The context.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   The response.
   */
  public function onBatchFinished(bool $success, array $results) {
    // Call the finish callbacks.
    $this->doBatchFinishedCallbacks($success, $results);

    // Redirect if needed.
    return $this->doBatchFinishedRedirection($success, $results);
  }

  /**
   * Call a list of defined callback.
   *
   * @param bool $success
   *   The succes status.
   * @param array $context
   *   The result data.
   */
  protected function doBatchFinishedCallbacks(bool $success, array $context) {
    $params = [
      'success' => $success,
      'context' => $context,
    ];
    foreach ($this->finishCallbacks as $callback) {
      $this->call($callback, $params);
    }
  }

  /**
   * Return redirection after batch finishes.
   *
   * @param bool $success
   *   The succes status.
   * @param array $context
   *   The context.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   The redirection.
   */
  protected function doBatchFinishedRedirection(bool $success, array $context) {
    $params = [
      'success' => $success,
      'context' => $context,
    ];

    return $this->call($this->redirectionCallback, $params);
  }

  /**
   * Return the list of operations.
   */
  protected function getOperationsList() {
    $wrapperdOperations = [];

    foreach ($this->operations as $operationData) {
      $wrapperdOperations[] = [
        [$this, 'doOperation'],
        [
          $operationData,
        ],
      ];
    }

    return $wrapperdOperations;
  }

  /**
   * Wrap the operation and pass the current batch.
   *
   * @param array $data
   *   The callback data.
   * @param array $context
   *   The context.
   *
   * @return mixed
   *   The response.
   */
  public function doOperation(array $data, array &$context) {
    return $this->call($data, $context);
  }

  /**
   * Call the callback.
   *
   * @param array $callbackData
   *   The callback data.
   * @param array|null $params
   *   Additionnal parameters.
   *
   * @return mixed
   *   The result.
   */
  protected function call(array $callbackData, array &$params = NULL) {
    $result = NULL;

    if (isset($callbackData['callback'])) {
      try {
        // First parameter is the specific operation data.
        $parameter = [$callbackData['params']];

        // Next parameter is context if defined.
        if (!is_null($params)) {
          $parameter[] = &$params;
        }

        // The last parameter is the current batch.
        $parameter[] = $this;

        $result = call_user_func_array(
          $callbackData['callback'],
          $parameter
        );
      }
      catch (\Exception $e) {
        $result = NULL;
      }
    }

    return $result;
  }

}
