<?php

namespace Drupal\config_entity_cloner\Form;

use Drupal\config_entity_cloner\Service\ConfigEntityCloner;
use Drupal\config_entity_cloner\Tools\BatchInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Configure Configuration Entity Type Cloner settings for this site.
 */
class CloneForm extends FormBase {

  /**
   * Field ID.
   *
   * @const string
   */
  const FIELD_ID = 'field_id';

  /**
   * Field Label.
   *
   * @const string
   */
  const FIELD_LABEL = 'field_label';

  /**
   * Route Matche.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Cloner.
   *
   * @var \Drupal\config_entity_cloner\Service\ConfigEntityCloner
   */
  protected $cloner;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Redirection.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $redirection;

  /**
   * Original entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $originalEntity;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CloneForm constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The Route matche.
   * @param \Drupal\config_entity_cloner\Service\ConfigEntityCloner $cloner
   *   The cloner.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $routeMatch, ConfigEntityCloner $cloner, MessengerInterface $messenger, EntityTypeManagerInterface $entityTypeManager) {
    $this->routeMatch = $routeMatch;
    $this->cloner = $cloner;
    $this->redirection = $this->routeMatch->getParameter('redirect');
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get(ConfigEntityCloner::SERVICE_NAME),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_entity_cloner_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Create form.
    $original = $this->getOriginalEntity();

    $title = $this->t('Duplicate @type @initial',
      [
        '@type'    => $original->getEntityType()->getLabel(),
        '@initial' => $original->label(),
      ]);
    $form['title']['#markup'] = '<h2>' . $title . '</h2>';

    $form[static::FIELD_LABEL] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('New entity label'),
      '#default_value' => $this->cloner->getDefaultCloneEntityLabel($original),
      '#required'      => TRUE,
    ];

    $form[static::FIELD_ID] = [
      '#type'             => 'machine_name',
      '#title'            => $this->t('New entity machine name'),
      '#default_value'    => $this->cloner->getDefaultCloneEntityId($original),
      '#element_validate' => [
        [$this, 'validateId'],
      ],
      '#required'         => TRUE,
    ];

    $form['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Duplicate'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Validate the availability of the id.
   *
   * @param array $element
   *   THe element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   THe formstate.
   */
  public function validateId(array $element, FormStateInterface $formState) {
    $newBundle = $formState->getValue(static::FIELD_ID);
    $existing = $this->cloner->getConfigEntityByTypeAndId(
      $this->getOriginalEntity()->getEntityTypeId(),
      $newBundle
    );
    if ($existing) {
      $formState->setError($element, $this->t('@bundler bundle name already exists',
        [
          '@bundle' => $newBundle,
        ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $originalEntity = $this->getOriginalEntity();

    $batch = $this->cloner->getCloneBatch(
      $form_state->getValue(static::FIELD_LABEL),
      $form_state->getValue(static::FIELD_ID),
      $originalEntity);

    // Add finish callbacks.
    $batch->addFinishCallback(
      get_called_class() . '::onBatchFinished'
    );

    // Set redirection.
    $batch->setRedirectionCallback(
      get_called_class() . '::getBatchFinishedRedirection',
      ['redirection' => $this->getRedirection($originalEntity->getEntityTypeId(), $form_state->getValue(static::FIELD_ID))]
    );

    $batch->init();
  }

  /**
   * Return the original entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The original entity.
   */
  public function getOriginalEntity() {
    if (is_null($this->originalEntity)) {
      $this->originalEntity = $this->cloner->getConfigEntityByTypeAndId(
        $this->routeMatch->getParameter('type'),
        $this->routeMatch->getParameter('initial_bundle')
      );
    }

    return $this->originalEntity;
  }

  /**
   * Actions on batch finished.
   *
   * @param array $data
   *   THe data.
   * @param array $context
   *   The context.
   * @param \Drupal\config_entity_cloner\Tools\BatchInterface $batch
   *   THe batch object.
   */
  public static function onBatchFinished(array $data, array $context, BatchInterface $batch) {
    $newEntityData = $batch->getCommonBatchData('newEntity');
    \Drupal::messenger()
      ->addMessage(t('The entity @label [@id] has been cloned', [
        '@label' => $newEntityData['label'],
        '@id'    => $newEntityData['id'],
      ]));
  }

  /**
   * Return the redirection after cloning.
   *
   * @param array $data
   *   THe data.
   * @param array $context
   *   The context.
   * @param \Drupal\config_entity_cloner\Tools\BatchInterface $batch
   *   THe batch object.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public static function getBatchFinishedRedirection(array $data, array $context, BatchInterface $batch) {
    return new RedirectResponse($data['redirection']);
  }

  /**
   * Return the redirection to the edit-form if exists.
   *
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $entityId
   *   The entity id.
   *
   * @return string
   *   THe url.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRedirection($entityTypeId, $entityId): string {
    $url = FALSE;

    // Check if a edit form url is available.
    $def = $this->entityTypeManager->getDefinition($entityTypeId);
    if ($def->hasLinkTemplate('edit-form')) {
      $replaceData = [
        '{' . $entityTypeId . '}' => $entityId,
      ];
      $urlInput = str_replace(array_keys($replaceData), $replaceData, $def->getLinkTemplate('edit-form'));
      try {
        $url = Url::fromUserInput($urlInput)->toString();
      }
      catch (\Exception $e) {
        $url = FALSE;
      }
    }

    return $url ?: Url::fromRoute($this->redirection)->toString();
  }

}
