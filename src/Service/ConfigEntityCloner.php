<?php

namespace Drupal\config_entity_cloner\Service;

use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessPluginManager;
use Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorPluginManager;
use Drupal\config_entity_cloner\Tools\Batch;
use Drupal\config_entity_cloner\Tools\BatchInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Cloner service.
 */
class ConfigEntityCloner {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Clone Processor manager.
   *
   * @var \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorPluginManager
   */
  protected ConfigEntityClonerProcessorPluginManager $cloneProcessor;

  /**
   * Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'config_entity_cloner.cloner';

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Constructs a Cloner object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcessor\ConfigEntityClonerProcessorPluginManager $cloneProcessor
   *   The clone processor plugin manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $moduleHandler,
    ConfigEntityClonerProcessorPluginManager $cloneProcessor
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $moduleHandler;
    $this->cloneProcessor = $cloneProcessor;
  }

  /**
   * Return the configuration entity by type and bundleName.
   *
   * @param string $type
   *   The type.
   * @param string $id
   *   The entity id.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getConfigEntityByTypeAndId($type, $id) {
    $entityTypeStorage = $this->entityTypeManager->getStorage($type);
    $entityType = $entityTypeStorage->getEntityType();

    return call_user_func($entityType->getClass() . '::load', $id);
  }

  /**
   * Return the configuration entity by type and bundleName.
   *
   * @param string $type
   *   The type.
   * @param string $label
   *   The entity id.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities list.
   */
  public function getConfigEntitiesByTypeAndLabel($type, $label) {
    $entityTypeStorage = $this->entityTypeManager->getStorage($type);
    /** @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface $entityType */
    $entityType = $entityTypeStorage->getEntityType();
    $labelKey = $entityType->getKey('label');
    $ids = $this->entityTypeManager->getStorage($entityType->id())
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition($labelKey, $label)->execute();

    return call_user_func($entityType->getClass() . '::loadMultiple', $ids);
  }

  /**
   * Return the next default entity id.
   *
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original entity.
   * @param int $maxTry
   *   The number of try max.
   * @param int $from
   *   The first suffix number.
   *
   * @return string
   *   The entity id.
   */
  public function getDefaultCloneEntityId(EntityInterface $originalEntity, $maxTry = 10, $from = 1): string {
    // Define the base id.
    $baseId = $originalEntity->id();
    $entityTypeId = $originalEntity->getEntityTypeId();
    do {
      $baseId = $this->getNextDefaultElement($baseId, $from++, '_');
      $continue = !is_null($this->getConfigEntityByTypeAndId($entityTypeId, $baseId));
    } while ($continue && $maxTry-- > 0);

    return $baseId;
  }

  /**
   * Return the next default entity id.
   *
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original entity.
   * @param int $maxTry
   *   The number of try max.
   * @param int $from
   *   The first suffix number.
   *
   * @return string
   *   The entity id.
   */
  public function getDefaultCloneEntityLabel(EntityInterface $originalEntity, $maxTry = 10, $from = 1): string {
    // Define the base id.
    $baseLabel = $originalEntity->label();
    $entityTypeId = $originalEntity->getEntityTypeId();
    do {
      $baseLabel = $this->getNextDefaultElement($baseLabel, $from++, ' ');
      $continue = count($this->getConfigEntitiesByTypeAndLabel($entityTypeId, $baseLabel)) > 0;
    } while ($continue && $maxTry-- > 0);

    return $baseLabel;
  }

  /**
   * Return the next default supposed name of an element.
   *
   * @param string $baseId
   *   The base name.
   * @param int $from
   *   The default number.
   * @param string $separator
   *   The separator.
   *
   * @return string
   *   The next default name.
   */
  protected function getNextDefaultElement($baseId, $from, $separator): string {
    $baseIdData = explode($separator, $baseId);
    if (is_numeric(end($baseIdData))) {
      $from = array_pop($baseIdData);
      $baseId = implode($separator, $baseIdData);
    }

    return $baseId . $separator . ($from + 1);
  }

  /**
   * Clone the configuration entity.
   *
   * @param string $newEntityName
   *   The entity name.
   * @param string $newEntityMachineName
   *   The entity machine name.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The clone entity.
   */
  public function cloneConfiguration($newEntityName, $newEntityMachineName, EntityInterface $originalEntity) {
    // Create clone.
    $newEntity = $this->duplicateEntity($newEntityName, $newEntityMachineName, $originalEntity);

    $this->processCloneEntity($newEntity, $originalEntity);

    return $newEntity;
  }

  /**
   * Process duplication.
   *
   * @param \Drupal\Core\Entity\EntityInterface $newEntity
   *   The new entity.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original entity.
   */
  public function processCloneEntity(EntityInterface $newEntity, EntityInterface $originalEntity) {
    foreach ($this->getListOfProcess($newEntity, $originalEntity) as $process) {
      $process->getProcess()->cloneProcess($newEntity, $originalEntity);
    }
  }

  /**
   * Clone the configuration entity.
   *
   * @param string $newEntityName
   *   The entity name.
   * @param string $newEntityMachineName
   *   The entity machine name.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original entity.
   * @param array $finishCallbacks
   *   The list of callbacks.
   * @param array $redirectionCallback
   *   The redirection callback.
   *
   * @return \Drupal\config_entity_cloner\Tools\BatchInterface
   *   The bach process.
   */
  public function getCloneBatch(
    $newEntityName,
    $newEntityMachineName,
    EntityInterface $originalEntity,
    array $finishCallbacks = [],
    array $redirectionCallback = []
  ): BatchInterface {
    $newEntity = $this->duplicateEntity($newEntityName, $newEntityMachineName, $originalEntity);

    $batch = (new Batch('Clone config entity...'))
      ->setInitMessage('Start')
      ->setProgressMessage('Processed @current out of @total.')
      ->setErrorMessage('An error occurred during processing')
      ->addCommonBatchData('newEntity',
                           [
                             'type' => $newEntity->getEntityTypeId(),
                             'id' => $newEntity->id(),
                             'label' => $newEntity->label(),
                           ])
      ->addCommonBatchData('originalEntity',
                           [
                             'type' => $originalEntity->getEntityTypeId(),
                             'id' => $originalEntity->id(),
                             'label' => $originalEntity->label(),
                           ]);

    // Add the operations.
    foreach ($this->getListOfProcess($newEntity, $originalEntity) as $process) {
      $batch->addOperation(
        get_called_class() . '::doCloneProcess',
        [
          'processId' => $process->getId(),
        ]
      );
    }

    return $batch;
  }

  /**
   * Return the list of processses.
   *
   * @param \Drupal\Core\Entity\EntityInterface $newEntity
   *   The new entity.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original entity.
   *
   * @return array
   *   The list of processes.
   */
  public function getListOfProcess(EntityInterface $newEntity, EntityInterface $originalEntity) {
    return $this->cloneProcessor
      ->getProcessorByEntity($originalEntity)
      ->getProcessList();
  }

  /**
   * Duplicate the entity.
   *
   * @param string $newEntityName
   *   The entity name.
   * @param string $newEntityMachineName
   *   The entity machine name.
   * @param \Drupal\Core\Entity\EntityInterface $originalEntity
   *   The original entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The clone entity.
   */
  public function duplicateEntity(string $newEntityName, string $newEntityMachineName, EntityInterface $originalEntity) {
    $newBundle = $originalEntity->createDuplicate();
    $keys = $newBundle->getEntityType()->getKeys();
    $newBundle->set($keys['id'], $newEntityMachineName);
    $newBundle->set($keys['label'], $newEntityName);
    $newBundle->save();

    return $newBundle;
  }

  /**
   * Callabck for cloning the entity in batch process.
   *
   * @param array $data
   *   The data.
   * @param array $context
   *   The context data.
   * @param \Drupal\config_entity_cloner\Tools\BatchInterface $batch
   *   The batch.
   */
  public static function doCloneProcess(array $data, array &$context, BatchInterface $batch) {
    $cloner = static::me();
    $context['results']['input_data'] = $data;

    // Init data.
    /** @var \Drupal\config_entity_cloner\PluginManager\ConfigEntityClonerProcess\ConfigEntityClonerProcessPluginWrapper $process */
    $process = ConfigEntityClonerProcessPluginManager::me()
      ->getProcessById($data['processId']);
    $newEntityData = $batch->getCommonBatchData('newEntity');
    $newEntity = $cloner->getConfigEntityByTypeAndId($newEntityData['type'], $newEntityData['id']);
    $originalEntityData = $batch->getCommonBatchData('originalEntity');
    $originalEntity = $cloner->getConfigEntityByTypeAndId($originalEntityData['type'], $originalEntityData['id']);

    try {
      $context['message'] = $process->getLabel();
      $process->getPlugin()->cloneProcess($newEntity, $originalEntity);
    }
    catch (\Exception $e) {
      // Mute exception...
    }
  }

}
