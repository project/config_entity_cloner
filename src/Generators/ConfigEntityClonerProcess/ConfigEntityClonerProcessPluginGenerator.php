<?php

namespace Drupal\config_entity_cloner\Generators\ConfigEntityClonerProcess;

use Drupal\config_entity_cloner\Generators\Base\AbstractPluginGenerator;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides generator of entity cloner process plugin.
 *
 * @package Drupal\config_entity_cloner\Generators
 */
class ConfigEntityClonerProcessPluginGenerator extends AbstractPluginGenerator {

  /**
   * The command name.
   *
   * @var string
   */
  protected string $name = 'config-entity-cloner-process-plugin';

  /**
   * The description.
   *
   * @var string
   */
  protected string $description = 'Generates a ConfigEntityClonerProcess plugin.';

  /**
   * The alias.
   *
   * @var string
   */
  protected string $alias = 'config-entity-cloner-process';

  /**
   * The template dir path.
   *
   * @var string
   */
  protected string $templatePath = __DIR__;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * ConfigEntityClonerProcessPluginGenerator constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface|null $moduleHandler
   *   The module handler.
   * @param string|null $name
   *   The name.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler = NULL, $name = NULL) {
    parent::__construct($name);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars): void {
    $this->collectDefault($vars);

    // Création du plugin.
    $this->addFile(
      'src/Plugin/config_entity_cloner/ConfigEntityClonerProcess/' . $vars['class'] . 'ConfigEntityClonerProcess.php',
      'templates/plugin.twig');
  }

}
