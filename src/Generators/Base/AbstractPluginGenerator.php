<?php

namespace Drupal\config_entity_cloner\Generators\Base;

use DrupalCodeGenerator\Command\Plugin\PluginGenerator;

/**
 * Provides abastract generator of plugin.
 *
 * @package Drupal\config_entity_cloner\Generators
 */
abstract class AbstractPluginGenerator extends PluginGenerator {

  /**
   * {@inheritdoc}
   */
  protected function collectDefault(array &$vars): void {
    parent::collectDefault($vars);
    // Plugin weight.
    $vars['plugin_weight'] = $this->ask('Plugin weight', 30, '::validateInt');
  }

  /**
   * Validates a int value.
   *
   * @param string|null $value
   *   A value to validate.
   *
   * @return string|null
   *   The validated value.
   */
  public static function validateInt(?string $value): ?string {
    if (is_numeric($value) && intval($value) == $value) {
      return $value;
    }
    throw new \UnexpectedValueException('Not an int');
  }

}
