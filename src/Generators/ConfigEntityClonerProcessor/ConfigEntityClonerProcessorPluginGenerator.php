<?php

namespace Drupal\config_entity_cloner\Generators\ConfigEntityClonerProcessor;

use Drupal\config_entity_cloner\Generators\Base\AbstractPluginGenerator;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides generator of config entity cloner processor.
 *
 * @package Drupal\config_entity_cloner\Generators
 */
class ConfigEntityClonerProcessorPluginGenerator extends AbstractPluginGenerator {

  /**
   * The command name.
   *
   * @var string
   */
  protected string $name = 'config-entity-cloner-processor-plugin';

  /**
   * The description.
   *
   * @var string
   */
  protected string $description = 'Generates a ConfigEntityClonerProcessor plugin.';

  /**
   * The alias.
   *
   * @var string
   */
  protected string $alias = 'config-entity-cloner-processor';

  /**
   * The template dir path.
   *
   * @var string
   */
  protected string $templatePath = __DIR__;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * ConfigEntityClonerProcessorPluginGenerator constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface|null $moduleHandler
   *   The module handler.
   * @param string|null $name
   *   Then name.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler = NULL, $name = NULL) {
    parent::__construct($name);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars): void {
    $this->collectDefault($vars);

    // Création du plugin.
    $this->addFile(
      'src/Plugin/config_entity_cloner/ConfigEntityClonerProcessor/' . $vars['class'] . 'ConfigEntityClonerProcessor.php',
      'templates/plugin.twig'
    );
  }

}
